﻿using UnityEngine;
public class BulletController : MonoBehaviour
{
    BulletDetai _detai;
    bool _allowMove = false;
    private void OnEnable()
    {
        GameController.ChangeStatus += ChangeGameStutas;
    }
    private void OnDisable()
    {
        Debug.Log(gameObject.name);
        GameController.ChangeStatus -= ChangeGameStutas;
    }
    private void Awake()
    {
        _detai = GetComponent<BulletDetai>();
    }
    private void Start()
    {
        _allowMove = (GameController.api.CurrentStatus == GAME_STATUS.Playing) ? true : false;
        Destroy(gameObject, 1.5f);
    }
    private void Update()
    {
        if (_allowMove || GameController.api.CurrentStatus == GAME_STATUS.Playing)
        {
            Move();
        }
    }
    void Move()
    {
        transform.position += transform.forward * _detai.Data.SpeedMoveBullet * Time.deltaTime;
    }
    void ChangeGameStutas(GAME_STATUS gAME_STATUS)
    {
        Debug.Log("afdsgdfg");
        if (gAME_STATUS == GAME_STATUS.Playing)
        {
            _allowMove = true;
        }
        else
        {
            _allowMove = false;
        }
    }
}
