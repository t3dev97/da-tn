﻿using UnityEngine;

public class BulletCollider : MonoBehaviour
{
    BulletData Data;
    private void Awake()
    {
        Data = GetComponent<BulletDetai>().Data;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Zombie")
        {
            // Tạo hiệu ứng ZB bị trúng đạn
            GCache.api.GetFXPrefabs(FX_NAME.HitParticles.ToString(), (fx) =>
            {
                GameObject t = Instantiate(fx, transform.position + Vector3.up, Quaternion.identity);
                t.transform.SetParent(GameplayController.api.ContainerFX, false);
            });

            // Trừ máu ZB
            other.GetComponent<ZombieController>().Hit(Data.DamageBullet);
        }
        // Xóa viên đạn này
        Destroy(gameObject);
    }
}
