﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;

public class AdsController : MonoBehaviour
{
    BannerView bannerView;
    AdRequest request;
    private void Awake()
    {
        MobileAds.Initialize(initStatus => { });
        if (!RuntimeManager.IsInitialized())
        {
            RuntimeManager.Init();
        }
        DontDestroyOnLoad(this);
    }
    private void Start()
    {
        request = new AdRequest.Builder().Build();
        bannerView = new BannerView("ca-app-pub-1163931047782398/7981415650", AdSize.Banner, 0, 50);

        // Load the banner with the request.
       
    }
    private void Update()
    {
        // Create an empty ad request.
        bannerView.LoadAd(request);
    }
}
