﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIEndGame : MonoBehaviour
{
    public TextMeshProUGUI TmpCMT;
    public Button BtnEG_Back;
    public Button BtnEG_Share;

    private void OnEnable()
    {
        //GameController.ChangeStatus += ChangeGameState;
        string s = (GameplayController.api.Win) ? "WIN" : "LOSE";
        TmpCMT.text = s;
    }
    private void OnDisable()
    {
        //GameController.ChangeStatus -= ChangeGameState;
    }

    private void ChangeGameState(GAME_STATUS vNew)
    {
        switch (vNew)
        {
            case GAME_STATUS.Pre:
                break;
            case GAME_STATUS.Map:
                break;
            case GAME_STATUS.PrePlay:
                break;
            case GAME_STATUS.Playing:
                break;
            case GAME_STATUS.Pause:
                break;
            case GAME_STATUS.Over:
                string s = (GameplayController.api.Win) ? "WIN" : "LOSE";
                TmpCMT.text = s;
                break;
        }
    }
    private void Start()
    {
        BtnEG_Back.onClick.AddListener(() =>
        {
            GetComponentInParent<UIBattleController>().EventPause();
        });
        BtnEG_Share.onClick.AddListener(() =>
        {
            GetComponentInParent<UIBattleController>().EventShare();
        });
    }
}