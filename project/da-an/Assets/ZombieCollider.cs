﻿using UnityEngine;

public partial class ZombieController : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Floor")
            _onFloor = true;

        if (other.transform.tag == "Player")
        {
            other.transform.GetComponent<PlayerController>().Hit(CurrentDamage);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Floor")
        {
            _onFloor = false;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
    }
}
