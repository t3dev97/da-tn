﻿using System;
using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static event Action<GAME_STATUS> ChangeStatus;
    public static GameController api;

    [SerializeField]
    GAME_STATUS _currentStatus;

    GameObject _gameplay;

    public GAME_STATUS CurrentStatus { get => _currentStatus; set => _currentStatus = value; }

    private void Awake()
    {
        if (api == null)
        {
            api = this;
        }
        DontDestroyOnLoad(this);
    }
    private void OnEnable()
    {
        SceneController.CurrentSceneName += ChangeScene;
        ChangeStatus += ChangeGameStatus;
    }
    private void OnDisable()
    {
        SceneController.CurrentSceneName -= ChangeScene;
        ChangeStatus -= ChangeGameStatus;
    }
    private void Start()
    {
        _currentStatus = GAME_STATUS.Pre;
        CreaterLoading();
    }

    void CreaterLoading()
    {
        GameObject t = new GameObject();
        t.name = "GCache";
        t.AddComponent<GCache>();

        t = new GameObject();
        t.name = "Loading";
        t.AddComponent<LoadFileData>();

        t = new GameObject();
        t.name = "SceneController";
        t.AddComponent<SceneController>();
    }

    void ChangeScene(string currenScene)
    {
        if (currenScene == "Main")
        {
            StartCoroutine(ShowUIMain());
        }
    }
    public IEnumerator ShowUIMain()
    {
        yield return new WaitForSeconds(.1f);
        UIController.api.ShowUI(UI_NAME.Main);
        UIController.api.ShowUI(UI_NAME.System);
    }

    public void ChangeGameStatus(GAME_STATUS vNew)
    {
        if (_currentStatus != vNew)
        {
            _currentStatus = vNew;

            switch (vNew)
            {
                case GAME_STATUS.Pre:
                    {

                    }
                    break;
                case GAME_STATUS.Map:
                    {

                    }
                    break;
                case GAME_STATUS.PrePlay:
                    {
                        Debug.Log("PrePlay");
                        _gameplay = new GameObject();
                        _gameplay.name = "GameplayController";
                        _gameplay.AddComponent<GameplayController>();
                    }
                    break;
                case GAME_STATUS.Playing:
                    {

                    }
                    break;
                case GAME_STATUS.Pause:
                    {

                    }
                    break;
                case GAME_STATUS.Over:
                    {

                    }
                    break;
            }
            ChangeStatus(vNew);
        }
    }
    public bool IsStatus(GAME_STATUS _STATUS)
    {
        return _currentStatus == _STATUS;
    }
    public void GetDataMap(MapData data)
    {
        _gameplay.name = data.NameMap;
        _gameplay.GetComponent<GameplayController>().mapData = data;
    }
    public void ClearGameplay()
    {
        Destroy(_gameplay.gameObject);
    }

}
