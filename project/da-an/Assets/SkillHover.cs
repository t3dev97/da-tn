﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillHover : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static event Action<float> ClickShoot;
   
    [SerializeField]
    bool _selec = false;
    float _timeLive = 0;
    public SKILL_NAME SkillName;

    private void OnEnable()
    {
        //ClickShoot += Shoot;
        //DontShoot += EventDontShoot;
    }
    private void OnDisable()
    {
        //ClickShoot -= Shoot;
        //DontShoot -= EventDontShoot;
    }
    public void OnPointerDown(PointerEventData eventData)
    {

        //_selec = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _timeLive = 0;
        //_selec = false;
    }
    private void Update()
    {
        //if (_selec && GameController.api.CurrentStatus == GAME_STATUS.Playing)
        //{
        //    _timeLive += Time.deltaTime;
        //    if (Input.GetKeyDown(KeyCode.Space))
        //    {
        //        Debug.Log("sdf");
        //        Shoot();
        //    }
        //    //ClickShoot(_timeLive);
        //}
        //else
        //{
        //    //DontShoot();
        //}
        if(_selec)
        {
            PlayerController.api.ResetRotateAvata();
            _selec = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("sdf");
            Shoot();
        }

    }
    void EventDontShoot()
    {

    }

    public void Shoot()
    {
        if (GameController.api.CurrentStatus == GAME_STATUS.Playing)
        {
            ClickShoot(_timeLive);
            StartCoroutine("Delay");
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(.5f);
        _selec = true;
    }
}
