﻿using UnityEngine;

public class GunController : MonoBehaviour
{
    public static event System.Action DontShoot;
    float _timeLive = 0;
    float _timeLast = 0;

    GunDetail _detail;
    public Transform PosBullet;
    private void OnEnable()
    {
        SkillHover.ClickShoot += Shoot;
    }
    private void OnDisable()
    {
        SkillHover.ClickShoot -= Shoot;
    }
    private void Awake()
    {
        _detail = GetComponent<GunDetail>();
    }
    private void Start()
    {
        PosBullet = transform.parent.GetComponentInChildren<PosBullet>().transform;
    }
    public void Shoot(float timeLive)
    {
        if (timeLive - _timeLast > _detail.CurrentCountDown || timeLive < 0.03f)
        {
            _timeLast = timeLive;

            //    Debug.Log("Shoot");
            var angle = new Vector3(0, transform.eulerAngles.y, 0);
            GameObject t = Instantiate(_detail.Bullet, PosBullet.position, Quaternion.Euler(angle), GameplayController.api.ContainerBullet);

            GCache.api.GetFXPrefabs(FX_NAME.GunParticles.ToString(), (fx) =>
            {
                GameObject f = Instantiate(fx, PosBullet.transform.position, Quaternion.identity);
                f.transform.localEulerAngles = new Vector3(0, PosBullet.transform.eulerAngles.y, 0);
                f.transform.SetParent(GameplayController.api.ContainerFX, true);
            });

            t.gameObject.SetActive(true);
        }
        //else
        //{
        //    if(DontShoot != null)
        //        DontShoot();
        //}
    }
}
