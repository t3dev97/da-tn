﻿using UnityEngine;
using UnityEngine.UI;

public class UIPause : MonoBehaviour
{
    public Button BtnPlay;
    private void Start()
    {
        BtnPlay.onClick.AddListener(() =>
        {
            GameplayController.api.Resume();
        });
    }
    private void OnEnable()
    {
        GameController.api.ChangeGameStatus(GAME_STATUS.Pause);
    }
}
