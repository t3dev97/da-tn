﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public partial class ZombieController : MonoBehaviour
{
    float _currentHP;
    float _currentSpeedMove;
    float _currentDamage;

    [SerializeField]
    bool _allowMove;

    [SerializeField]
    bool _onFloor;

    NavMeshAgent _agent;

    [SerializeField]
    Transform _player;

    public ZombieData Data;

    public Slider SldHP;

    public float CurrentHP
    {
        get => _currentHP; set
        {
            _currentHP = value;
            if (SldHP != null)
            {
                SldHP.value = _currentHP / Data.MaxHP;
            }
        }
    }
    public float CurrentSpeedMove { get => _currentSpeedMove; set => _currentSpeedMove = value; }
    public float CurrentDamage { get => _currentDamage; set => _currentDamage = value; }
}
