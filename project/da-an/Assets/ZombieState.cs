﻿using System;
using UnityEngine;
using UnityEngine.AI;

public partial class ZombieController : MonoBehaviour
{
    bool _allowHit = true;
    void MoveToTargetAI(Transform tg, NavMeshAgent agent, float speed)
    {
        agent.enabled = true;
        agent.speed = speed;
        agent.SetDestination(tg.position);
    }
    void Die()
    {
        _allowHit = false;
        _allowMove = false;
        if (_agent != null)
            _agent.enabled = false;

        GameplayController.api.CurrentNumberZBDie++;

        Destroy(gameObject, 0.1f);
    }
    public void Hit(float damageTake, Action action = null)
    {

        if (_allowHit)
        {
            CurrentHP -= damageTake;
            CurrentHP = Mathf.Clamp(CurrentHP, 0, CurrentHP);

            if (action != null)
                action();

            if (CurrentHP <= 0)
            {
                Die();
            }
        }
    }

}
