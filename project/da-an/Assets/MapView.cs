﻿using UnityEngine;
using UnityEngine.UI;

public class MapView : MonoBehaviour
{
    public Image Img;
    public Text NameMap;

    public MapData data;
    public void SetupData(int id)
    {
        data = GCache.api.MapDatas[id];
    }
}
