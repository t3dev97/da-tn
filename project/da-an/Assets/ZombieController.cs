﻿using UnityEngine;
using UnityEngine.AI;

public partial class ZombieController : MonoBehaviour
{
    private void Awake()
    {
        SetUp();
    }
    private void Start()
    {
        SldHP = GetComponentInChildren<HPBarController>().SldHP;
        SldHP.value = CurrentHP / Data.MaxHP;

        if (GameController.api.CurrentStatus == GAME_STATUS.Playing)
            _allowMove = true;
    }
    private void OnEnable()
    {
        GameController.ChangeStatus += ChangeGameStatus;
    }
    private void OnDisable()
    {
        GameController.ChangeStatus -= ChangeGameStatus;
    }
    private void Update()
    {
        if (_allowMove && _onFloor)
        {
            if (_player == null)
                _player = GameObject.FindWithTag("Player").transform;
            MoveToTargetAI(_player, _agent, Data.SpeedMove);
        }
    }
    public void SetUp()
    {
        CurrentHP = Data.MaxHP;
        CurrentDamage = Data.Damage;
        CurrentSpeedMove = Data.SpeedMove;
        _agent = GetComponent<NavMeshAgent>();
    }
    void ChangeGameStatus(GAME_STATUS vNew)
    {
        if (vNew == GAME_STATUS.Playing)
            _allowMove = true;
        else
            _allowMove = false;
    }
}
