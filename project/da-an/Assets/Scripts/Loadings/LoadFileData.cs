﻿using System;
using System.Collections;
using UnityEngine;
public class LoadFileData : MonoBehaviour
{
    int _numberProcce = 7;
    private void Start()
    {
        StartCoroutine(LoadData((float v) =>
        {
            UILoadingReference.api.LoadingBar.value = v;
        }, () =>
        {
            SceneController.api.LoadScene(ContainerText.SceneName.Main);
        }));
    }
    IEnumerator LoadData(Action<float> actionPess = null, Action actionComplete = null)
    {
        int index = 0;
        #region Load Data
        var data = Resources.LoadAll(ContainerText.Link.Path_CharacterData, typeof(CharacterData));
        foreach (CharacterData tg in data)
        {
            //GCache.api.ListCharacterData.Add(tg);
            GCache.api.CharacterDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.CharacterDatas.Count + " CharacterDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_MapData, typeof(MapData));
        foreach (MapData tg in data)
        {
            //GCache.api.ListMapData.Add(tg);
            GCache.api.MapDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.MapDatas.Count + " MapDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_ZombieData, typeof(ZombieData));
        foreach (ZombieData tg in data)
        {
            //GCache.api.ListZombieData.Add(tg);
            GCache.api.ZombieDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.ZombieDatas.Count + " ZombieDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_GunData, typeof(GunData));
        foreach (GunData tg in data)
        {
            //GCache.api.ListGunData.Add(tg);
            GCache.api.GunDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.GunDatas.Count + " GunDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_ItemData, typeof(ItemData));
        foreach (ItemData tg in data)
        {
            //GCache.api.ListItemData.Add(tg);
            GCache.api.ItemDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.ItemDatas.Count + " ItemDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_PlaneData, typeof(PlaneData));
        foreach (PlaneData tg in data)
        {
            //GCache.api.ListPlaneData.Add(tg);
            GCache.api.PlaneDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce); 
        Debug.Log("Load " + GCache.api.PlaneDatas.Count + " PlaneDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_BulletData, typeof(BulletData));
        foreach (BulletData tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            GCache.api.BulletDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.BulletDatas.Count + " BulletDatas done");

        data = Resources.LoadAll(ContainerText.Link.Path_UserData, typeof(UsersData));
        foreach (UsersData tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            GCache.api.UserDatas.Add(tg.Id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.UserDatas.Count + " UserDatas done");
        #endregion

        #region Load Prefabs
        data = Resources.LoadAll(ContainerText.Link.Path_PlayerPrefab, typeof(GameObject));
        foreach (GameObject tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            int id = tg.GetComponent<PlayerDetail>().Data.Id;
            GCache.api.PlayerPrefabs.Add(id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.PlayerPrefabs.Count + " PlayerPrefabs done");

        data = Resources.LoadAll(ContainerText.Link.Path_GunPrefab, typeof(GameObject));
        foreach (GameObject tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            int id = tg.GetComponent<GunDetail>().Data.Id;
            GCache.api.GunPrefabs.Add(id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.GunPrefabs.Count + " GunPrefabs done");

        data = Resources.LoadAll(ContainerText.Link.Path_BulletPrefab, typeof(GameObject));
        foreach (GameObject tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            int id = tg.GetComponent<BulletDetai>().Data.Id;
            GCache.api.BulletPrefabs.Add(id, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.BulletPrefabs.Count + " BulletPrefabs done");

        data = Resources.LoadAll(ContainerText.Link.Path_FXPrefab, typeof(GameObject));
        foreach (GameObject tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            //int id = tg.GetComponent<BulletDetai>().Data.Id;
            GCache.api.FXPrefabs.Add(tg.name, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.FXPrefabs.Count + " FXPrefabs done");
        #endregion

        #region Audio
        data = Resources.LoadAll(ContainerText.Link.Path_AudioFX, typeof(AudioClip));
        foreach (AudioClip tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            //int id = tg.GetComponent<BulletDetai>().Data.Id;
            GCache.api.AudioFXs.Add(tg.name, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.AudioFXs.Count + " AudioFX done");

        data = Resources.LoadAll(ContainerText.Link.Path_Music, typeof(AudioClip));
        foreach (AudioClip tg in data)
        {
            //GCache.api.ListBulletData.Add(tg);
            //int id = tg.GetComponent<BulletDetai>().Data.Id;
            GCache.api.Musics.Add(tg.name, tg);
        }
        if (actionPess != null)
            actionPess(++index / _numberProcce);
        Debug.Log("Load " + GCache.api.Musics.Count + " Musics done");
        #endregion

        Debug.Log("-------------Load data done-----------------");
        yield return null;
        if (actionComplete != null)
            actionComplete();
    }
}
