﻿using System.Collections.Generic;
using UnityEngine;

public class Models : MonoBehaviour
{

}
[SerializeField]
public class PlayerDataTest
{
    public int Hp;
    public int MaxHp;
    public Vector3 Position;
    public string Name;
   

    public PlayerDataTest(int hp, int maxHp, Vector3 position)
    {
        Hp = hp;
        MaxHp = maxHp;
        Position = position;
    }

}
public class OneTimeGameplay
{
    public int Id; // số thứ tự của đợt
    public int NumberZombie; // số zb của đợt
    public List<int> listIDPosZB; // danh sách vị trí zombie, theo thứ tự zombie
    public List<GameObject> listZB; // danh sách ZB, theo thứ tự zombie
}