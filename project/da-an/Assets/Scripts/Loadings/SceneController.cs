﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController api;
    public static event Action<string> CurrentSceneName;
    private void Awake()
    {
        if (api == null)
            api = this;
        DontDestroyOnLoad(this);
    }
    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name, LoadSceneMode.Single);
        CurrentSceneName(name);
    }
    public string CurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }
}
