﻿public static class ContainerEnum
{

}
public enum TYPE_GUN
{
    NormalGun,
    LazerGun,
    ShortGun
}
public enum UI_NAME
{
    Main = 0,
    System = 1,
    Battle = 2,
    Maps = 3
}
public enum GAME_STATUS
{
    Pre = 0, // ngoài lobby
    Map = 1, // chọn map
    PrePlay = 2, // chuẩn bị vào map
    Playing = 3, // đang trong trận
    Pause = 4, // ngừng
    Over = 5 // kết thúc màn 
}
public enum SKILL_NAME
{
    Shoot
}
public enum FX_NAME
{
    GunParticles,
    HitParticles,
    Lights
}
public enum AUIDO_CLIP_NAM
{
    BG, // nhac background
    HellephantDeath,
    HellephantHurt,
    PlayerDeath,
    PlayerGunShot,
    PlayerHurt,
    ZomBearDeath,
    ZomBearHurt,
    ZomBunnyDeath,
    ZomBunnyHurt
}
