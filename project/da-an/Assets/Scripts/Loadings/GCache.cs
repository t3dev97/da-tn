﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GCache : MonoBehaviour
{
    public static GCache api;

    //[SerializeField]
    //List<CharacterData> _listCharacterData;

    //[SerializeField]
    //List<MapData> _listMapData;

    //[SerializeField]
    //List<ZombieData> _listZombieData;

    //[SerializeField]
    //List<GunData> _listGunData;

    //[SerializeField]
    //List<ItemData> _listItemData;

    //[SerializeField]
    //List<PlaneData> _listPlaneData;

    //[SerializeField]
    //List<BulletData> _listBulletData;

    #region Data
    public Dictionary<int, CharacterData> CharacterDatas = new Dictionary<int, CharacterData>();
    public Dictionary<int, MapData> MapDatas = new Dictionary<int, MapData>();
    public Dictionary<int, ZombieData> ZombieDatas = new Dictionary<int, ZombieData>();
    public Dictionary<int, GunData> GunDatas = new Dictionary<int, GunData>();
    public Dictionary<int, ItemData> ItemDatas = new Dictionary<int, ItemData>();
    public Dictionary<int, PlaneData> PlaneDatas = new Dictionary<int, PlaneData>();
    public Dictionary<int, BulletData> BulletDatas = new Dictionary<int, BulletData>();
    public Dictionary<int, UsersData> UserDatas = new Dictionary<int, UsersData>();
    #endregion

    #region Prefabs
    public Dictionary<int, GameObject> PlayerPrefabs = new Dictionary<int, GameObject>();
    public Dictionary<int, GameObject> GunPrefabs = new Dictionary<int, GameObject>();
    public Dictionary<int, GameObject> BulletPrefabs = new Dictionary<int, GameObject>();
    public Dictionary<string, GameObject> FXPrefabs = new Dictionary<string, GameObject>();
    #endregion

    public Dictionary<string, AudioClip> AudioFXs = new Dictionary<string, AudioClip>();
    public Dictionary<string, AudioClip> Musics = new Dictionary<string, AudioClip>();


    //public List<CharacterData> ListCharacterData { get => _listCharacterData; set => _listCharacterData = value; }
    //public List<MapData> ListMapData { get => _listMapData; set => _listMapData = value; }
    //public List<ZombieData> ListZombieData { get => _listZombieData; set => _listZombieData = value; }
    //public List<GunData> ListGunData { get => _listGunData; set => _listGunData = value; }
    //public List<ItemData> ListItemData { get => _listItemData; set => _listItemData = value; }
    //public List<PlaneData> ListPlaneData { get => _listPlaneData; set => _listPlaneData = value; }
    //public List<BulletData> ListBulletData { get => _listBulletData; set => _listBulletData = value; }

    private void Awake()
    {
        if (api == null)
            api = this;
        DontDestroyOnLoad(this);
    }
    public void GetFXPrefabs(string name, Action<GameObject> fx = null)
    {
        if (fx != null)
        {
            fx(FXPrefabs[name]);
        }
    }
}
