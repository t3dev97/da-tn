﻿public static class ContainerText
{
    public static class AnimName
    {
        public const string Idle = "Idle";
        public const string Run = "Run";
        public const string Hit = "Hit";
        public const string Atk = "Atk";
        public const string Die = "Die";
        public const string Hit_Tr = "Hit_Tr";
        public const string Die_Tr = "Die_Tr";
    }

    public static class Link
    {
        public const string Datas = "Datas/";
        public const string Prefabs = "Prefabs/";
        public const string Audio = "Audio/";
        
        #region link Data
        public const string Path_CharacterData = Datas + "Characters";
        public const string Path_MapData = Datas + "Maps";
        public const string Path_ZombieData = Datas + "Zombies";
        public const string Path_GunData = Datas + "Guns";
        public const string Path_ItemData = Datas + "Items";
        public const string Path_PlaneData = Datas + "Planes";
        public const string Path_BulletData = Datas + "Bullets";
        public const string Path_UserData= Datas + "Users";
        #endregion

        #region link Prefab
        public const string Path_PlayerPrefab = Prefabs + "Players";
        public const string Path_GunPrefab = Prefabs + "Guns";
        public const string Path_BulletPrefab = Prefabs + "Bullets";
        public const string Path_FXPrefab = Prefabs + "FXs";
        public const string Path_AudioFX = Audio + "Effects";
        public const string Path_Music = Audio + "Music";
        #endregion
    }

    public static class SceneName
    {
        public const string Main = "Main";
    }
}
