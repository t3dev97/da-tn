﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "New Data/User Data", fileName = "New User")]
public class UsersData : ScriptableObject
{
    public int Id;
    public string NameUser;
    public int IDCharacter;
    public int IDGun;
}
