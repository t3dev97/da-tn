﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Item Data", fileName = "New Item")]
public class ItemData : ScriptableObject
{
    public int Id;
    public string NameItem;
    public string Des;
    public Sprite Avata;
    public int UpHP;
    public int UpDamage;
    public int UpSpeedMove;
    public int UpSpeedAtk;
}
