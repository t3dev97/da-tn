﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Zombie Data", fileName = "New Zombie")]
public class ZombieData : ScriptableObject
{
    public int Id;
    public string Name;
    public float MaxHP;
    public float SpeedMove;
    public float Damage;
    public GameObject Model;
}
