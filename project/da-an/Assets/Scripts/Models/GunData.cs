﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Gun Data", fileName = "New Gun")]
public class GunData : ScriptableObject
{
    public int Id;
    public string NameGun;
    public string Des;

    [Header("Setting Gun")]
    public TYPE_GUN TypeGun; // loại Gun (Thường, Lazer, Short)
    public int NumberRay; // số tia
    public float AngleRay; // số gốc của mỗi Bullet tạo ra
    public float Countdown; // Thời gian giản cách giữa những viên đạn
    public float Mass; // trọng lượng của Gun, làm giảm khả năng di chuyển của Player
    public float PushBack; // độ đẫy lùi của Gun khi bắn (Đẩy Player)
    public int FXShoot; // ID của Effect khi bắn 
    public GameObject Model;

    [Header("Setting Bullet")]
    public int IdBullet; // ID của Bullet

}
