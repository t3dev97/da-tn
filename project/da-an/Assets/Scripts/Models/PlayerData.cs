﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Player Data", fileName = "New Player")]
public class PlayerData : ScriptableObject
{
    public int Id;
    public int Level;

    public int IdCharacter;
    public int IdGun;
}
