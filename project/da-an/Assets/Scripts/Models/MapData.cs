﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Map Data", fileName = "New Map")]
public class MapData : ScriptableObject
{
    public int Id;
    public string NameMap;
    public string Des;
    public bool IsUnlock;
    public int NumberStar;

    [Header("Setting ZB")]
    public int MaxZombie;
    public int MinZombieOneTime; // số quái tối thiểu trong 1 đợt ra quân
    public int MaxZombieOneTime; // số quái cao nhất trong 1 đợt ra quân
    public float MinTimeCreate; // thời gian nhanh nhất để ra quân
    public float MaxTimeCreate; // thời gian chậm nhất để ra quân
    public List<GameObject> ListZombieNormal; // danh sách Id các zombie sẻ xuất hiện 
    public List<GameObject> ListZombieBoss; // danh sách Id các zombie sẻ xuất hiện 
    public float UpToHPZombie; // số HP cộng thêm 
    public float UpToDamageZombie; // số Damage cộng thêm

    [Header("Setting Item Drop")]
    public List<int> ListIDItemNormal; // danh sách Id các vật phẩm thường sẻ rơi
    public List<int> ListIDItemSpencial; // danh sách Id các vật phẩm đặc biệt sẻ rơi
    public float RateNormal; // tỷ lệ rơi vật phâm thường
    public float RateSpecial; // tỷ lệ rơi vật phẩm đặc biệt
    
    [Header("Setting BG")]
    public int Plane; // Id Plane của Map
}
