﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Character Data", fileName = "New Character")]
public class CharacterData : ScriptableObject
{
    public int Id;
    public string Name;
    public float MaxHP;
    public float SpeedMove;
    public GameObject Model; //skin
}
