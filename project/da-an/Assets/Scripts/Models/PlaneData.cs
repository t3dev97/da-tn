﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Plane Data", fileName = "New Plane")]
public class PlaneData : ScriptableObject
{
    public int Id;
    public string NamePlane;
    public GameObject Model;
}
