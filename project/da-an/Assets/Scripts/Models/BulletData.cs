﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Data/Bullet Data", fileName = "Bullet Item")]
public class BulletData : ScriptableObject
{
    public int Id;
    public string NameBullet;
    public float DamageBullet; // 
    public float SpeedMoveBullet; // 
    public float PushBackBullet; //  độ đẩy lui Monster khi trúng (Đẩy Monster)
    public GameObject Model;
}
