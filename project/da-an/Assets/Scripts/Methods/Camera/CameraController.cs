﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController api;
    CameraShake _cameraShake;
    CameraFollowPlayer _cameraFollowPlayer;
    private void Awake()
    {
        if (api == null)
        {
            api = this;
        }

        _cameraShake = GetComponent<CameraShake>();
        _cameraFollowPlayer = GetComponent<CameraFollowPlayer>();
    }
    public void SetupCamera(GameObject pl, bool allowMove)
    {
        _cameraFollowPlayer.Setup(pl, allowMove);
    }
    public void AllowFollowPlayer(bool v)
    {
        _cameraFollowPlayer.AllowMove = v;
    }
    public void Shake()
    {
        _cameraShake.ShouldShake = true;
    }
}
