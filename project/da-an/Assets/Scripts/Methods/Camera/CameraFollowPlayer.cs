﻿using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    [SerializeField]
    bool _allowMove;

    public GameObject player;
    public Vector3 OffsetPos;
    public float SpeedMove;

    public bool AllowMove { get => _allowMove; set => _allowMove = value; }
    private void OnEnable()
    {
        PlayerMove.PlayerMoving += FollowPlayer;
    }
    private void OnDisable()
    {
        PlayerMove.PlayerMoving -= FollowPlayer;
    }
    public void Setup(GameObject pl, bool allowMove)
    {
        player = pl;
        _allowMove = allowMove;
    }
    void FollowPlayer(bool AllowMove)
    {
        if (AllowMove)
        {
            Vector3 vec = OffsetPos + player.transform.position;
            gameObject.transform.position = Vector3.Lerp(transform.position, vec, SpeedMove * Time.deltaTime);
            //transform.LookAt(player.transform.position);
        }
    }
}
