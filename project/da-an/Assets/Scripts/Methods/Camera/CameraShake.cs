﻿using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float Power = 0.2f;
    public float Duration = 0.1f;
    public float SlowDownAmount = 1.0f;
    public bool ShouldShake = false;

    Vector3 _startPostion;
    float _InitialDuration;
    private void Start()
    {
        _InitialDuration = Duration;
        _startPostion = transform.localPosition;
    }
    private void Update()
    {
        if (ShouldShake)
        {
            if (Duration > 0)
            {
                transform.localPosition += Random.insideUnitSphere * Power;
                Duration -= Time.deltaTime * SlowDownAmount;
            }
            else
            {
                ShouldShake = false;
                Duration = _InitialDuration;
            }
        }
    }
}
