﻿using EasyMobile;
using System;
using System.Collections;
using UnityEngine;

public partial class PlayerController : MonoBehaviour
{
    bool _allowHit = true;

    public bool AllowHit
    {
        get
        {
            if (GameController.api.CurrentStatus == GAME_STATUS.Playing)
                return _allowHit;
            return false;
        }
        set => _allowHit = value;
    }

    void Die()
    {
        AllowMove = false;
        GameController.api.ChangeGameStatus(GAME_STATUS.Over);
    }
    IEnumerator DelayTakeHit(float t)
    {
        AllowHit = false;
        yield return new WaitForSeconds(t);
        AllowHit = true;
    }

    public void Hit(float damageTake, Action action = null)
    {
        if (AllowHit)
        {
            Debug.Log("Player Hit");
            MyDetail.CurrentHP -= damageTake;
            MyDetail.CurrentHP = Mathf.Clamp(MyDetail.CurrentHP, 0, MyDetail.CurrentHP);

            if (action != null)
                action();

            if (MyDetail.CurrentHP <= 0)
            {
                StartCoroutine(SaveScreenshot());
                Die();
            }
            StartCoroutine(DelayTakeHit(1));
        }
    }
    IEnumerator SaveScreenshot()
    {
        // Chờ đến cuối frame
        yield return new WaitForEndOfFrame();

        // Lưu ảnh với đường dẫn
        string path = Sharing.SaveScreenshot("screenshot");
    }
}
