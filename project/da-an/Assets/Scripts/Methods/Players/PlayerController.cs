﻿using UnityEngine;

public partial class PlayerController : MonoBehaviour
{
    public static PlayerController api;
    public PlayerDetail MyDetail;
    public PlayerAnimationController playerAnimation;

    public bool AllowMove = false;
    public bool IsMoving = false;

    private void Awake()
    {
        if (api == null)
            api = this;
        playerAnimation = GetComponent<PlayerAnimationController>();
        MyDetail = GetComponent<PlayerDetail>();
    }
    private void OnEnable()
    {
        GameController.ChangeStatus += ChangeGameStatu;
        SkillHover.ClickShoot += LookZombie;
        //PlayerMove.PlayerMoving += ResetRotateAvata;
        //GunController.DontShoot += ResetRotateAvata;
    }


    private void OnDisable()
    {
        GameController.ChangeStatus -= ChangeGameStatu;
        SkillHover.ClickShoot -= LookZombie;
        //PlayerMove.PlayerMoving -= ResetRotateAvata;
        //GunController.DontShoot -= ResetRotateAvata;
    }
    public void ResetRotateAvata()
    {
        MyDetail.Avata.transform.localEulerAngles = new Vector3(0, 0, 0);
    }
    private void LookZombie(float obj)
    {
        Debug.Log("Look ZB");
        MyDetail.Avata.transform.LookAt(GameplayController.api.Target);
    }

    void ChangeGameStatu(GAME_STATUS vNew)
    {
        if (vNew == GAME_STATUS.Playing)
        {
            AllowMove = true;
            AllowHit = true;
        }
        else
        {
            AllowMove = false;
            AllowHit = false;
        }
    }
}
