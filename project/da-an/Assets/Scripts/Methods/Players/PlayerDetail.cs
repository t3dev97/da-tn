﻿using UnityEngine;

public class PlayerDetail : MonoBehaviour
{
    float _currentHP;
    float _maxHP;
    float _currentSpeedMove;

    // Data luu
    int _idCharacter = 0; // Character sử dụng trong gameplay
    int _idGun = 0; // Character sử dụng trong gameplay
    public PlayerData Data;
    public GameObject Avata;
    public GameObject Gun;
    public Transform PosGun;

    public float CurrentHP
    {
        get => _currentHP;
        set
        {
            _currentHP = value;
            UIBattleView.api.SldHPPlayer.value = _currentHP / _maxHP;
        }
    }
    public float MaxHP { get => _maxHP; set => _maxHP = value; }
    public float CurrentSpeedMove { get => _currentSpeedMove; set => _currentSpeedMove = value; }

    public void Init()
    {
        _idCharacter = GCache.api.UserDatas[1].IDCharacter;
        _idGun = GCache.api.UserDatas[1].IDGun;

        MaxHP = GCache.api.CharacterDatas[_idCharacter].MaxHP;
        CurrentHP = GCache.api.CharacterDatas[_idCharacter].MaxHP;
        CurrentSpeedMove = GCache.api.CharacterDatas[_idCharacter].SpeedMove;

        Avata = Instantiate(GCache.api.CharacterDatas[_idCharacter].Model, transform, false);
        PosGun = transform.GetComponentInChildren<PosGun>().transform;

        Gun = Instantiate(GCache.api.GunPrefabs[_idGun], PosGun.transform, false);
    }
}
