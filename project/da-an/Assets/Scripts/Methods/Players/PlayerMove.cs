﻿using System;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public static event Action<bool> PlayerMoving;

    [SerializeField]
    Joystick _joyStickControl;

    PlayerDetail _myDetail;
    PlayerController _controller;
    Rigidbody _rb;

    float _disCanMove = 0.3f;
    private void Awake()
    {
        _myDetail = GetComponent<PlayerDetail>();
        _controller = GetComponent<PlayerController>();
        _rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        _joyStickControl = GameObject.Find("JoystickMove").GetComponent<Joystick>();
    }
    void Update()
    {
        if (_controller.AllowMove)
        {
            Move();
        }
    }
    void Move()
    {
        if (_joyStickControl.Horizontal > 0.1f || _joyStickControl.Horizontal < -0.1f
             || _joyStickControl.Vertical > 0.1f || _joyStickControl.Vertical < -0.1f)
        {
            Vector3 direction = new Vector3(_joyStickControl.Horizontal, 0f, _joyStickControl.Vertical).normalized;
            //Debug.Log(JoyStickControl.Horizontal);

            if (_joyStickControl.Horizontal > _disCanMove || _joyStickControl.Horizontal < -_disCanMove
              || _joyStickControl.Vertical > _disCanMove || _joyStickControl.Vertical < -_disCanMove)
                _rb.velocity = direction * _myDetail.CurrentSpeedMove;

            var angle = Vector2.Angle(_joyStickControl.Direction, Vector2.up);
            if (_joyStickControl.Horizontal < 0)
                angle = -angle;

            transform.rotation = Quaternion.Euler(0, angle, 0);

            //if (GameplayController.api.Target == Vector3.zero)
            //{
            //    Debug.Log("dfgdfg");
            //    _myDetail.Avata.transform.localEulerAngles = new Vector3(0, 0, 0);
            //}


            _controller.IsMoving = true;
            _controller.playerAnimation.aRun(ContainerText.AnimName.Run);
            PlayerMoving(true);
        }
        else
        {
            _controller.IsMoving = false;
            _controller.playerAnimation.aRun(ContainerText.AnimName.Idle);
        }
    }
}
