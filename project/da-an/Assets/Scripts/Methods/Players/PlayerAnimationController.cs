﻿using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField]
    PlayerController _controller;

    Animator _anim;
    string _currentAnim = "";
    private void Awake()
    {
        _controller = GetComponent<PlayerController>();
    }
    private void Start()
    {
        _anim = _controller.MyDetail.Avata.GetComponent<Animator>();
    }

    public void aRun(string name)
    {
        if (_currentAnim != "")
            _anim.SetBool(_currentAnim, false);
        _anim.SetBool(name, true);
        _currentAnim = name;
    }
}
