﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UIBattleController))]
public class UIBattleView : MonoBehaviour
{
    public static UIBattleView api;
    UIBattleController _battleController;
    public Button BtnPause;

    public GameObject PnlGamePause;

    public GameObject PnlEndGame;

    public Slider SldHPPlayer;

    public TextMeshProUGUI TxtNumberZB;

    private void Awake()
    {
        if (api == null)
            api = this;

        _battleController = GetComponent<UIBattleController>();
    }
    private void OnEnable()
    {
        GameController.ChangeStatus += ChangeGameState;
    }
    private void OnDisable()
    {
        GameController.ChangeStatus -= ChangeGameState;
    }
    private void Start()
    {
        AddControl();
        AddEvent();
    }

    private void AddControl()
    {
        BtnPause = GameObject.Find("BtnPause").GetComponent<Button>();
    }

    private void AddEvent()
    {
        BtnPause.onClick.AddListener(() =>
        {
            //_battleController.EventPause();
            GameController.api.ChangeGameStatus(GAME_STATUS.Pause);
        });
    }
    void ChangeGameState(GAME_STATUS vNew)
    {
        switch (vNew)
        {
            case GAME_STATUS.Pre:
                break;
            case GAME_STATUS.Map:
                break;
            case GAME_STATUS.PrePlay:
                PnlEndGame.SetActive(false);
                break;
            case GAME_STATUS.Playing:
                PnlEndGame.SetActive(false);
                PnlGamePause.SetActive(false);
                break;
            case GAME_STATUS.Pause:
                PnlGamePause.SetActive(true);
                break;
            case GAME_STATUS.Over:
                PnlEndGame.SetActive(true);
                break;
        }
    }
}