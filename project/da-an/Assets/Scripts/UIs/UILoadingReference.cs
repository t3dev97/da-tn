﻿using UnityEngine;
using UnityEngine.UI;

public class UILoadingReference : MonoBehaviour
{
    public static UILoadingReference api;

    public Slider LoadingBar;
    private void Awake()
    {
        if (api == null)
            api = this;
    }
}
