﻿using UnityEngine;
using UnityEngine.UI;

public class TextShadow : MonoBehaviour
{
    [SerializeField]
    string _textValue;

    Text _txtShadow;

    Text _txtMain;

    public string TextValue
    {
        get => _textValue;
        set
        {
            _textValue = value;
            _txtShadow.text = _txtMain.text = _textValue.ToString();
        }
    }

    private void Awake()
    {
        _txtShadow = transform.GetChild(0).GetComponent<Text>();
        _txtMain = transform.GetChild(1).GetComponent<Text>();
    }
    private void Start()
    {
        if (_textValue != "")
            _txtShadow.text = _txtMain.text = _textValue.ToString();
    }
}
