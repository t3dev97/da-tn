﻿using System;
using System.Collections;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController api;
    [SerializeField]
    GameObject _uiMain;

    [SerializeField]
    GameObject _uiSystem;

    [SerializeField]
    GameObject _uiBattle;

    [SerializeField]
    GameObject _uiMaps;

    UI_NAME _currentUI;

    public GameObject UiMain { get => _uiMain; set => _uiMain = value; }
    public GameObject UiSystem { get => _uiSystem; set => _uiSystem = value; }
    public GameObject UiBattle { get => _uiBattle; set => _uiBattle = value; }
    public GameObject UiMaps { get => _uiMaps; set => _uiMaps = value; }

    private void Awake()
    {
        if (api == null)
            api = this;
    }
    private void Start()
    {
        GetUI();
    }
    void GetUI()
    {
        _uiMain = transform.GetChild(0).gameObject;
        _uiSystem = transform.GetChild(1).gameObject;
        _uiBattle = transform.GetChild(2).gameObject;
        _uiMaps = transform.GetChild(3).gameObject;
    }
    public void ShowUI(UI_NAME uI_NAME, Action actionBefore = null, Action actionAffter = null)
    {
        if (actionBefore != null)
            actionBefore();
        if (transform.GetChild((int)uI_NAME).gameObject.activeSelf == false)
            transform.GetChild((int)uI_NAME).gameObject.SetActive(true);

        _currentUI = uI_NAME;
        if (actionAffter != null)
            actionAffter();
    }
    public void HideUI(UI_NAME uI_NAME, Action actionBefore = null, Action actionAffter = null)
    {
        if (actionBefore != null)
            actionBefore();

        if (transform.GetChild((int)uI_NAME).gameObject.activeSelf)
            transform.GetChild((int)uI_NAME).gameObject.SetActive(false);

        if (actionAffter != null)
            actionAffter();
    }
    public IEnumerator EventFromMainToMaps()
    {
        yield return new WaitForSeconds(.1f);
        HideUI(UI_NAME.Main, null, () =>
        {
            ShowUI(UI_NAME.Maps, null, () =>
            {
                StartCoroutine(_uiMaps.GetComponent<UIMapController>().SetupData());
            });
        });
    }
    public IEnumerator EventBackFromMapToMain(UIMapController uIMapController)
    {
        yield return new WaitForSeconds(.1f);
        HideUI(UI_NAME.Maps, () =>
        {
            uIMapController.ClearMaps();
        }, () =>
        {
            ShowUI(UI_NAME.Main);
        });
    }
    public IEnumerator EventMapToBattle(UIMapController uIMapController, MapData map)
    {
        yield return new WaitForSeconds(.1f);
        ShowUI(UI_NAME.Battle, () =>
        {
            HideUI(UI_NAME.Maps, () =>
            {
                uIMapController.ClearMaps();
            });
        }, () =>
        {
            GameController.api.ChangeGameStatus(GAME_STATUS.PrePlay);
            GameController.api.GetDataMap(map);
        });
    }
    public IEnumerator EventBattleToMain(UIBattleController uIBattleController)
    {
        yield return new WaitForSeconds(.1f);
        ShowUI(UI_NAME.Main, () =>
        {
            GameController.api.ClearGameplay();
            GameController.api.ChangeGameStatus(GAME_STATUS.Pre);
        }, () =>
        {
            HideUI(UI_NAME.Battle);
        });
    }
    //private void OnEnable()
    //{
    //    GameController.ChangeStatus += ChangeGameStatus;
    //}
    //private void OnDisable()
    //{
    //    GameController.ChangeStatus -= ChangeGameStatus;
    //}
    //public void ChangeGameStatus(GAME_STATUS vNew)
    //{
    //    Debug.Log(vNew.ToString());
    //}
}
