﻿using EasyMobile;
using System.Collections;
using UnityEngine;

public class UIBattleController : MonoBehaviour
{
    public void EventPause()
    {
        StartCoroutine(UIController.api.EventBattleToMain(this));
    }

    public void EventShare()
    {
        Debug.Log("Share");
        // Share a saved image
        // Suppose we have a "screenshot.png" image stored in the persistentDataPath,
        // we'll construct its path first
        string path = System.IO.Path.Combine(Application.persistentDataPath, "screenshot.png");

        // Share the image with the path, a sample message and an empty subject
        Sharing.ShareImage(path, "This is a sample message");
        Sharing.ShareURL("www.google.com");
        Sharing.ShareText("SMS");
    }
   
}
