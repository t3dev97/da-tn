﻿using EasyMobile;
using UnityEngine;
using UnityEngine.UI;

public class UIMainView : MonoBehaviour
{
    UIMainController _mainController;

    public GameObject Level;
    public GameObject Gold;
    public GameObject Exp;
    public GameObject TxtPlayerName;
    public Slider SliderEXP;
    public Button BtnDailyQuests;
    public Button BtnSettings;
    public Button BtnPlay;
    public Button BtnAds;
    public Button BtnShopWeapons;
    public Button BtnShopHeroes;

    private void Awake()
    {
        _mainController = GetComponent<UIMainController>();
    }
    private void Start()
    {
        AddControl();
        AddEvent();
    }
    void AddControl()
    {
        Level = GameObject.Find("Level");
        Gold = GameObject.Find("Gold");
        Exp = GameObject.Find("Exp");
        TxtPlayerName = GameObject.Find("TxtPlayerName");
        SliderEXP = GameObject.Find("SliderEXP").GetComponent<Slider>();
        BtnDailyQuests = GameObject.Find("BtnDailyQuests").GetComponent<Button>();
        BtnSettings = GameObject.Find("BtnSettings").GetComponent<Button>();
        BtnPlay = GameObject.Find("BtnPlay").GetComponent<Button>();
        BtnAds = GameObject.Find("BtnAds").GetComponent<Button>();
        BtnShopWeapons = GameObject.Find("BtnShopWeapons").GetComponent<Button>();
        BtnShopHeroes = GameObject.Find("BtnShopHeroes").GetComponent<Button>();
    }
    void AddEvent()
    {
        BtnDailyQuests.onClick.AddListener(() =>
        {
            // nhiem vu hang ngay
            Debug.Log("Daily Quests");
        });

        BtnPlay.onClick.AddListener(() =>
        {
            //Debug.Log("Play");
            StartCoroutine(UIController.api.EventFromMainToMaps());
        });

        BtnAds.onClick.AddListener(() =>
        {
            Debug.Log("Ads");
            //StartCoroutine(UIController.api.EventFromMainToMaps());
            // Show banner ad
            Advertising.ShowBannerAd(BannerAdPosition.Bottom);
        });

        BtnSettings.onClick.AddListener(() =>
        {
            Debug.Log("Setting");
        });

        BtnShopHeroes.onClick.AddListener(() =>
        {
            // Shop Heros
            Debug.Log("Shop Heroes");
        });

        BtnShopWeapons.onClick.AddListener(() =>
        {
            // Shop Weapons
            Debug.Log("Shop Weapons");
        });
    }
    private void Update()
    {
        if (Advertising.IsInterstitialAdReady()){

        }
        if (Advertising.IsRewardedAdReady())
        {
            Debug.Log("sdfgsdf");
            Advertising.ShowRewardedAd();
        }
    }
}
