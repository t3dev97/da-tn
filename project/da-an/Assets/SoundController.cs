﻿using UnityEngine;

public class SoundController : MonoBehaviour
{
    public static SoundController api;

    private void Awake()
    {
        if (api == null) api = this;
        DontDestroyOnLoad(this);
    }
    public void StopMusic(AudioSource audioSource)
    {
        audioSource.Stop();
    }
}
