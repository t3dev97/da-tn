﻿using UnityEngine;

public class GunDetail : MonoBehaviour
{
    int _currentNumberRay;
    float _currentAngleRay;
    float _currentCountDown;
    float _currentDamage;
    public GunData Data;
    public GameObject Avata;

    //[HideInInspector]
    public GameObject Bullet;
    //public Transform PosBullet;

    public int CurrentNumberRay { get => _currentNumberRay; set => _currentNumberRay = value; }
    public float CurrentAngleRay { get => _currentAngleRay; set => _currentAngleRay = value; }
    public float CurrentCountDown { get => _currentCountDown; set => _currentCountDown = value; }
    public float CurrentDamage { get => _currentDamage; set => _currentDamage = value; }

    private void Awake()
    {
        SetUp(Data);
    }
    public void SetUp(GunData data)
    {
        Data = data;
        _currentNumberRay = data.NumberRay;
        _currentAngleRay = data.AngleRay;
        _currentCountDown = data.Countdown;
        Avata = Instantiate(data.Model, transform, false);
        Bullet = /*Instantiate(*/GCache.api.BulletPrefabs[data.IdBullet]/*)*/;
        Bullet.SetActive(false);
    }
}
