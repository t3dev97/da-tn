﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIMapController : MonoBehaviour
{
    [SerializeField]
    Button _btnBack;

    [SerializeField]
    Transform _listMap;

    [SerializeField]
    GameObject _mapDemo;

    private void Start()
    {
        AddEventButton();
    }
    void AddEventButton()
    {
        _btnBack.onClick.AddListener(() =>
        {
            StartCoroutine(UIController.api.EventBackFromMapToMain(this));
        });
    }

    public IEnumerator SetupData()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject t;
        foreach (var item in GCache.api.MapDatas)
        {
            //Debug.Log(item.Value.NameMap);
            t = Instantiate(_mapDemo, _listMap);
            t.name = item.Value.NameMap + "-" + item.Key;

            if (t.GetComponent<MapDetail>() == null)
                t.AddComponent<MapDetail>();
            t.GetComponent<MapDetail>().Data = item.Value;

            t.SetActive(true);

            t.GetComponent<Button>().onClick.AddListener(() =>
            {
                StartCoroutine(UIController.api.EventMapToBattle(this, item.Value));
            });
        }
        //Debug.Log(GCache.api.MapDatas.Count);

    }
    public bool ClearMaps()
    {
        foreach (Transform item in _listMap)
        {
            Destroy(item.gameObject);
        }
        return true;
    }
    IEnumerator ClearObj()
    {
        Destroy(_listMap.GetChild(1).gameObject);
        yield return new WaitForEndOfFrame();
    }
}
