﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    private static GameplayController _api;

    int _currentNumberZBDie; // số ZB die

    [SerializeField]
    GameObject _plane;

    [SerializeField]
    GameObject _player;

    List<OneTimeGameplay> _listGameplay; // thông tin đợt quái

    public List<GameObject> ListPlayer;
    public MapData mapData;
    public Transform ContainerBullet;
    public Transform ContainerFX;
    public Transform ContainerZombie;
    public List<Transform> ListPosCreateMonster;
    private Vector3 target;
    public bool Win = false;

    public static GameplayController api
    {
        get
        {
            return _api;
        }
    }

    public int CurrentNumberZBDie
    {
        get => _currentNumberZBDie;
        set
        {
            _currentNumberZBDie = value;
            if (_currentNumberZBDie >= mapData.MaxZombie)
            {
                Win = true;
                GameController.api.ChangeGameStatus(GAME_STATUS.Over);
            }
        }
    }

    public Vector3 Target { get => GetTarget(_player.transform, 2); set => target = value; }

    private void Awake()
    {
        if (_api == null)
            _api = this;
        ListPlayer = new List<GameObject>();
        ListPosCreateMonster = new List<Transform>();
        _listGameplay = new List<OneTimeGameplay>();
    }
    private void OnEnable()
    {
        GameController.ChangeStatus += ChangeGameStatus;
    }
    private void OnDisable()
    {
        GameController.ChangeStatus -= ChangeGameStatus;
    }
    private void Update()
    {
        //if (GameController.api.CurrentStatus == GAME_STATUS.Playing)
        //    Target = GetTarget(_player.transform, 2);
    }
    public void ChangeGameStatus(GAME_STATUS vNew)
    {
        switch (vNew)
        {
            case GAME_STATUS.Pre:
                break;
            case GAME_STATUS.Map:
                break;
            case GAME_STATUS.PrePlay:
                StartCoroutine(CreateMap());
                break;
            case GAME_STATUS.Playing:
                {
                    Time.timeScale = 1;
                }
                break;
            case GAME_STATUS.Pause:
                {
                    Time.timeScale = 0;
                }
                break;
            case GAME_STATUS.Over:
                SoundController.api.StopMusic(_plane.GetComponent<AudioSource>());
                break;
        }
    }

    public IEnumerator CreateMap()
    {
        yield return new WaitForSeconds(.1f);
        Debug.Log("Create Map");
        _plane = Instantiate(GCache.api.PlaneDatas[mapData.Plane].Model, this.transform);

        ListPosCreateMonster = GetPosCreateMonsters(_plane.transform);

        yield return new WaitForSeconds(.1f);

        _player = Instantiate(GCache.api.PlayerPrefabs[1], this.transform);
        _player.GetComponent<PlayerDetail>().Init();

        ListPlayer.Add(_player);
        CameraController.api.SetupCamera(_player, true);

        ContainerBullet = new GameObject("ContainerBullet").transform;
        ContainerBullet.SetParent(transform);

        ContainerFX = new GameObject("ContainerFX").transform;
        ContainerFX.SetParent(transform);

        ContainerZombie = new GameObject("ContainerZombie").transform;
        ContainerZombie.SetParent(transform);

        CreateZBGameplay();

        GameController.api.ChangeGameStatus(GAME_STATUS.Playing);
    }
    List<Transform> GetPosCreateMonsters(Transform bg)
    {
        List<Transform> t = new List<Transform>();
        foreach (Transform item in bg)
        {
            if (item.GetComponent<Pos>() != null)
                t.Add(item);
        }
        return t;
    }

    void CreateZBGameplay()
    {
        int numberZBWillCreate = 0; // số zb sẻ tạo trong đợt này
        int currentMaxNumberZB = mapData.MaxZombie; // số zb tối đa của map 
        int numberZBCanCreate = currentMaxNumberZB; // số zb có thể tạo còn lại

        int numberZBCreated = 0; // số zb đã tạo trong map
        int id = 0;
        int numberGameplay = 0;
        while (numberZBCanCreate > 0)
        {
            // tính toán thiết lập
            numberZBWillCreate = Random.Range(mapData.MinZombieOneTime, mapData.MaxZombieOneTime);
            numberZBCanCreate = currentMaxNumberZB - numberZBCreated;

            numberZBWillCreate = (numberZBWillCreate < numberZBCanCreate) ? numberZBWillCreate : numberZBCanCreate;
            numberZBCanCreate -= numberZBWillCreate;

            numberZBCreated += numberZBWillCreate;
            // cài đặt 1 đợt 
            OneTimeGameplay t = new OneTimeGameplay();
            t.Id = ++id;
            t.NumberZombie = numberZBWillCreate;

            t.listIDPosZB = new List<int>();
            t.listZB = new List<GameObject>();
            for (int i = 0; i < numberZBWillCreate; i++)
            {
                int indexPosCreate = 0;// thứ tự vị trí tạo 
                indexPosCreate = Random.Range(0, ListPosCreateMonster.Count);
                t.listIDPosZB.Add(indexPosCreate);

                int indexZB = 0;
                indexZB = Random.Range(0, mapData.ListZombieNormal.Count);
                t.listZB.Add(mapData.ListZombieNormal[indexZB]);
            }
            _listGameplay.Add(t);
            // hoàn thành thiết lập
            numberGameplay++;
        }
        Debug.Log("================ Thông tin màn chơi ===============");
        Debug.Log("Tổng ZB: " + mapData.MaxZombie);
        Debug.Log("Tổng số đợt: " + _listGameplay.Count);
        for (int i = 0; i < _listGameplay.Count; i++)
        {
            Debug.Log("Đợt " + (i + 1) + " [ " + _listGameplay[i].listZB.Count + "]");
        }

        StartCoroutine(CreateZB(_listGameplay.Count));
    }

    GameObject GetZombie(int round, int indexZB)
    {
        Debug.Log(ListPosCreateMonster[_listGameplay[round].listIDPosZB[indexZB]].position);
        GameObject t = Instantiate(_listGameplay[round].listZB[indexZB], ListPosCreateMonster[_listGameplay[round].listIDPosZB[indexZB]]);
        t.transform.SetParent(ContainerZombie, true);
        return t;
    }

    Vector3 GetTarget(Transform pl, float distance)
    {
        float s = 0;
        Vector3 v; // vị trí sau khi đồng nhất
        Vector3 result = Vector3.zero; // vị trí cuối cùng
        float sMax = 0;
        if (ContainerZombie.childCount <= 0) return result;
        foreach (Transform child in ContainerZombie)
        {
            v = child.position;
            v.y = pl.position.y;
            s = Vector3.Distance(v, child.position);

            if (s < distance && s > sMax)
            {
                result = v;
                sMax = s;
            }
        }
        return result;
    }
    IEnumerator CreateZB(int number) // number: là số đợt tạo quái
    {
        for (int round = 0; round < _listGameplay.Count; round++)
        {
            float t = Random.Range(mapData.MinTimeCreate, mapData.MaxTimeCreate);
            for (int index = 0; index < _listGameplay[round].listZB.Count; index++)
            {
                GetZombie(round, index);
                yield return new WaitForSeconds(t);
            }
            yield return new WaitForSeconds(_listGameplay[round].listZB.Count * t + 10);
        }
    }
    IEnumerator DelayPlaying()
    {
        yield return new WaitForSeconds(0f);
        GameController.api.ChangeGameStatus(GAME_STATUS.Playing);
    }

    public void Resume()
    {
        StartCoroutine(DelayPlaying());
    }
}
